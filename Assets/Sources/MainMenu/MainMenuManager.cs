﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    public void Update()
    {
        if (Input.GetKey("escape"))
        {
            Quit();
        }
    }

    public void StartGame()
    {
        ChangeSceneManager.Instance().ChangeScene(Scene.Game);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
