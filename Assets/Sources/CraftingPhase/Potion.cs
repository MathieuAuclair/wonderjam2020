﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion
{
    private List<Ingredient> ingredients = new List<Ingredient>();
    private Stats stats;

    public Potion()
    {
        ingredients = new List<Ingredient>();
        stats = new Stats();
    }

    public void AddIngredient(Ingredient ingredient)
    {
        stats.strength += ingredient.stats.strength;
        stats.dexterity += ingredient.stats.dexterity;
        stats.constitution += ingredient.stats.constitution;
        stats.speed += ingredient.stats.speed;
        ingredients.Add(ingredient);
    }

    public Stats GetStats()
    {
        return stats;
    }
}
