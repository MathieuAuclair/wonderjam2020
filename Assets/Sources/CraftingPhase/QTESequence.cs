﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QTESequence
{
    public enum QTEInput
    {
        ButtonA = 0,
        ButtonB = 1,
        ButtonX = 2,
        ButtonY = 3,
        DPadLeft = 4,
        DPadRight = 5,
        DPadDown = 6,
        DPadUp = 7,
        BumperLeft = 8,
        BumperRight = 9
    }

    [SerializeField]
    public List<QTEInput> code = new List<QTEInput>();

    public void Reset()
    {
        code.Clear();
    }

    public void Append(QTESequence sequence)
    {
        code.AddRange(sequence.code);
    }

    public void Append(QTEInput input)
    {
        code.Add(input);
    }

    public QTEInput GetAt(int index)
    {
        Debug.Log("index : " + index);
        return code[index];
    }

    public string GetAtAsString(int index)
    {
        return QTESequence.QTEInputToString(code[index]);
    }

    public int GetSize()
    {
        return code.Count;
    }

    public static string QTEInputToString(QTEInput qteInput)
    {
        switch (qteInput)
        {
            case QTEInput.ButtonA:
                return "ButtonA";
            case QTEInput.ButtonB:
                return "ButtonB";
            case QTEInput.ButtonX:
                return "ButtonX";
            case QTEInput.ButtonY:
                return "ButtonY";
            case QTEInput.DPadLeft:
                return "DPadLeft";
            case QTEInput.DPadRight:
                return "DPadRight";
            case QTEInput.DPadDown:
                return "DPadDown";
            case QTEInput.DPadUp:
                return "DPadUp";
            case QTEInput.BumperLeft:
                return "BumperLeft";
            case QTEInput.BumperRight:
                return "BumperRight";
            default:
                return "";
        }
    }

    public override string ToString()
    {
        string result = "";
        foreach(QTEInput input in code)
        {
            result += " " + QTEInputToString(input);
        }
        return result;
    }
}
