﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class PlayerManager : MonoBehaviour
{
  private static System.Random rnd = new System.Random();
  private static PlayerManager sInstance;
  public Ingredient[] ingredientsPlayerPool;

  public List<Ingredient> ingredientPoolTotal = new List<Ingredient>();


  [Range(1, 5)]
  public int numberMinItem;
  [Range(1, 5)]
  public int numberMaxItem;

  public int numberActualItem;
  public int MaxOutpost = 5;
  public int OutpostsRemaining;
  public int CurrentRound = 0;

  //Enemy won   if Winner == 1
  //Warrior won if Winner == 2
  public int Winner = 0;

  public static PlayerManager Instance()
  {
    return sInstance;
  }

  void Awake()
  {
    ingredientsPlayerPool = new Ingredient[5];
    Assert.IsTrue(numberMaxItem >= numberMinItem, "numberMinItem > numberMaxItem");
    DontDestroyOnLoad(gameObject);
    sInstance = this;
  }

  // Start is called before the first frame update
  void Start()
  {
    numberActualItem = numberMinItem;
    OutpostsRemaining = 3;
    InitIngredientPool();

  }

  public void AddIngredientToAvailablePool(Ingredient ingredient)
  {
    if (numberActualItem < numberMaxItem)
    {
      numberActualItem++;
    }
    for (int i = numberActualItem - 2; i >= 0; i--)
    {
      ingredientsPlayerPool[i + 1] = ingredientsPlayerPool[i];
      Debug.Log(ingredientsPlayerPool[i]);
      Debug.Log(ingredientsPlayerPool[i + 1]);
    }
    ingredientsPlayerPool[0] = ingredient;
  }

  public void InitIngredientPool()
  {
    for (int i = numberActualItem - 1; i >= 0; i--)
    {
      int r = rnd.Next(ingredientPoolTotal.Count);
      ingredientsPlayerPool[i] = ingredientPoolTotal[r];
    }
  }

  public int GetNumberActualItem()
  {
    return numberActualItem;
  }
  public void StartTurn()
  {
    Debug.Log("Start Turn !");
    reinitiateTurn();
    ChangeSceneManager.Instance().ChangeScene(Scene.Game);
  }

  public void WarriorWin()
  {
      OutpostsRemaining++;
      Winner = 2;
      Debug.Log("Warrior won ! " + OutpostsRemaining + " outpost remaining");
      ChangeSceneManager.Instance().ChangeScene(Scene.BattlePhaseScene);
  }

  public void EnemyWin()
  {
      OutpostsRemaining--;
      Winner = 1;
      Debug.Log("Ennemy won ! " + OutpostsRemaining + " outpost remaining");
      ChangeSceneManager.Instance().ChangeScene(Scene.BattlePhaseScene);
  }

  public void reinitiateTurn()
  {
    Winner = 0;
  }

  public bool isGameOver()
  {
    return OutpostsRemaining <= -1;
  }

  public bool isGameWinning()
  {
    return OutpostsRemaining >= MaxOutpost + 2;
  }
}
