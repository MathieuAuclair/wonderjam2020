﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerBar : MonoBehaviour
{
    public Image imageWarriorBase;
    public Image imageEnemyBase;
    public Image[] imageOutpost;
    public Sprite spriteWarriorBase;
    public Sprite spriteWarriorBaseBattle;
    public Sprite spriteEnemyBase;
    public Sprite spriteEnemyBaseBattle;
    public Sprite spriteOutpostWarrior;
    public Sprite spriteOutpostEnemy;
    public Sprite spriteOutpostBattle;

    // Start is called before the first frame update
    void Start()
    {
        if(PlayerManager.Instance().OutpostsRemaining == 0)
            imageWarriorBase.sprite = spriteWarriorBaseBattle;
        else
            imageWarriorBase.sprite = spriteWarriorBase;

        if(PlayerManager.Instance().OutpostsRemaining == PlayerManager.Instance().MaxOutpost + 1)
            imageEnemyBase.sprite = spriteEnemyBaseBattle;
        else
            imageEnemyBase.sprite = spriteEnemyBase;

        for(int i = 0; i < PlayerManager.Instance().OutpostsRemaining - 1; i++)
            imageOutpost[i].sprite = spriteOutpostWarrior;

        if(PlayerManager.Instance().OutpostsRemaining > 0 && PlayerManager.Instance().OutpostsRemaining < PlayerManager.Instance().MaxOutpost)
            imageOutpost[PlayerManager.Instance().OutpostsRemaining - 1].sprite = spriteOutpostBattle;

        for(int i = PlayerManager.Instance().OutpostsRemaining; i < PlayerManager.Instance().MaxOutpost; i++)
            imageOutpost[i].sprite = spriteOutpostEnemy;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
