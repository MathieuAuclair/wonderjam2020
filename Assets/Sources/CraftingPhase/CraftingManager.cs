﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingManager : MonoBehaviour
{
    public Potion currentPotion;
    private Ingredient currentlyAddedIngredient;
    private bool isCrafting;
    private bool isWaitingForValidatePotionSplashSreen;
    private float remainingCraftingTime;
    private float nextTimerAlertAt;
    public Text timerText;
    private static CraftingManager sInstance;

    public GameObject zoneToSpawnIngredientModel3D;

    public Animator druidAnimator;

    public Animator cauldronAnimator;
    public GameObject potionSplashScreen;

    public GradeHUD gradeHUD;
    private AudioSource audioSource;
    public AudioClip[] randomSoundList; 


    public static CraftingManager Instance()
    {
        return sInstance;
    }

    void Awake()
    {
        sInstance = this;
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        currentPotion = new Potion();
        currentlyAddedIngredient = null;
        isCrafting = false;


        StartCrafting(45);
    }

    // Update is called once per frame
    void Update()
    {
        if (isCrafting)
        {
            UpdateTimer();

            if (remainingCraftingTime <= 0.0f)
            {
                StopCrafting();
            }
        }

        else if (isWaitingForValidatePotionSplashSreen)
        {
            if(Input.GetButtonDown("ButtonA"))
            {
                ValidatePotionSplashScreen();
            }
        }
    }

    public void StartCrafting(float craftingMaxDuration)
    {
        if (isCrafting)
            return;

        Debug.Log("StartCrafting");
        PlayerManager.Instance().CurrentRound += 1;
        Debug.Log("Round : " + PlayerManager.Instance().CurrentRound);
        currentPotion = new Potion();
        InventoryHUDManager.Instance().UpdateHUD();
        currentlyAddedIngredient = null;
        remainingCraftingTime = craftingMaxDuration;
        timerText.text = remainingCraftingTime.ToString();
        nextTimerAlertAt = remainingCraftingTime - 1.0f;
        QTEManager.Instance().ResetForNewPotion();
        potionSplashScreen.SetActive(false);
        isWaitingForValidatePotionSplashSreen = false;
        isCrafting = true;
    }

    private void StopCrafting()
    {
        if (!isCrafting)
            return;

        Debug.Log("StopCraftingFromTimer");
        if (currentlyAddedIngredient != null)
        {
            QTEManager.Instance().AbortQTE();
            currentlyAddedIngredient = null;
        }
        potionSplashScreen.SetActive(true);
        isWaitingForValidatePotionSplashSreen = true;
        isCrafting = false;
        InventoryHUDManager.Instance().HideHUD();
    }

    public void ValidatePotionSplashScreen()
    {
        if(!isCrafting && isWaitingForValidatePotionSplashSreen)
        {
            StartBattle();
        }
    }

    public void StartBattle()
    {
        BattleManager.Instance().PrepareBattle(CraftingManager.Instance().GetPotionStats());
        BattleManager.Instance().Fight();
    }

    public void AddIngredientToPotion(Ingredient ingredient)
    {

        if (isCrafting && currentlyAddedIngredient == null)
        {
            druidAnimator.SetTrigger("AddItemEvent");
            GameObject go = Instantiate(ingredient.model3DPrefab, new Vector3(0, 0, 0), ingredient.model3DPrefab.transform.rotation) as GameObject;
            go.transform.SetParent(zoneToSpawnIngredientModel3D.transform);
            go.transform.localPosition = Vector3.zero;
            go.SetActive(true);
            Debug.Log("AddIngredientToPotion :  " + ingredient.name);
            currentlyAddedIngredient = ingredient;
            InventoryHUDManager.Instance().HideHUD();
            QTEManager.Instance().StartQTE(ingredient.qteSequence, ingredient.difficultyLevel);
            audioSource.PlayOneShot(randomSoundList[Random.Range(0, randomSoundList.Length)]);
        }
    }

    public void AddIngredientSuccess()
    {
        if (isCrafting && currentlyAddedIngredient != null)
        {
            Debug.Log("AddIngredientSuccess");
            druidAnimator.SetTrigger("FinishedMixing");
            switch (currentlyAddedIngredient.cauldronEffect)
            {
                case CauldronEffect.Strength:
                    cauldronAnimator.SetTrigger("Strength");
                    break;
                case CauldronEffect.Dexterity:
                    cauldronAnimator.SetTrigger("Dexterity");
                    break;
                case CauldronEffect.Constitution:
                    cauldronAnimator.SetTrigger("Constitution");
                    break;
                case CauldronEffect.Speed:
                    cauldronAnimator.SetTrigger("Speed");
                    break;
            }
            currentPotion.AddIngredient(currentlyAddedIngredient);
            currentlyAddedIngredient = null;
            gradeHUD.UpdateGrade();
            InventoryHUDManager.Instance().ShowHUD();
        }
    }

    public void AddIngredientFail()
    {
        if (isCrafting && currentlyAddedIngredient != null)
        {
            Debug.Log("AddIngredientFail");
            druidAnimator.SetTrigger("FinishedMixing");
            cauldronAnimator.SetTrigger("Fail");
            currentlyAddedIngredient = null;
            InventoryHUDManager.Instance().ShowHUD();
        }
    }

    private void UpdateTimer()
    {
        remainingCraftingTime -= Time.deltaTime;

        if (remainingCraftingTime <= nextTimerAlertAt)
        {
            Debug.Log(nextTimerAlertAt + " seconds remaining");
            timerText.text = nextTimerAlertAt.ToString();
            nextTimerAlertAt -= 1.0f;
        }
    }

    public Stats GetPotionStats()
    {
        return currentPotion.GetStats();
    }

    public bool IsCraftingFinished()
    {
        return !isCrafting;
    }

    public bool IsInQTE()
    {
        return currentlyAddedIngredient != null;
    }
}
