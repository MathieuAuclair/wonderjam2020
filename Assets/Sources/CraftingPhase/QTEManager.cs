﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QTEManager : MonoBehaviour
{

    [SerializeField]
    private bool QTERunning;
    private QTESequence currentSequence = new QTESequence();
    private int currentDifficulty;
    private QTESequence baseSequence = new QTESequence();
    private int nextInputCodeIndex;
    private string currentInput;
    private string currentInputAxis;
    private bool isCurrentInputAnAxis;
    private bool isCurrentInputTreated;

    public Text remainingLineText;
    public Image[] inputImages;
    public int maxDisplayInputCount = 5;
    public Sprite[] completeInputSpriteList;
    public Sprite noInputSprite;
    public AudioClip soundError;
    public float volumeError = 1.0f;
    public GameObject particuleError;
    public AudioClip soundProgress;
    public float volumeProgress = 1.0f;
    public GameObject particuleProgress;
    public AudioClip soundSuccess;
    public float volumeSuccess = 1.0f;
    public GameObject particuleSuccess;
    private AudioSource audioSource;
    
    private static QTEManager sInstance;

    public static QTEManager Instance()
    {
        return sInstance;
    }

    void Awake()
    {
        sInstance = this;
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        QTERunning = false;
        ResetCurrentInput();
    }

    // Update is called once per frame
    void Update()
    {
        if(QTERunning)
        {
            TreatInputDown();
            TreatInputUp();
            TreatCurrentInput();
        }
    }

    public void ResetForNewPotion()
    {
        Debug.Log("ResetForNewPotion");
        baseSequence.Reset();
        currentSequence.Reset();
        nextInputCodeIndex = 0;
        UpdateQTEUI();
    }

    public void StartQTE(QTESequence newSequence, int difficultyLevel)
    {
        Debug.Log("StartQTE");
        currentSequence.Reset();
        currentSequence.Append(baseSequence);
        currentSequence.Append(newSequence);
        currentDifficulty = difficultyLevel;
        nextInputCodeIndex = 0;
        ResetCurrentInput();
        currentInput = "ButtonA";
        isCurrentInputAnAxis = false;
        isCurrentInputTreated = true;
        Debug.Log(currentSequence.ToString());
        UpdateQTEUI();
        QTERunning = true;
    }

    private void StopQTE()
    {
        Debug.Log("StopQTE");
        currentSequence.Reset();
        nextInputCodeIndex = 0;
        ResetCurrentInput();
        UpdateQTEUI();
        QTERunning = false;
    }

    public void AbortQTE()
    {
        Debug.Log("AbortQTE");
        CraftingManager.Instance().AddIngredientFail();
        StopQTE();
    }

    private void ProgressQTE()
    {
        if(!IsLastInputForCurrentSequence())
            ProgressFeedBack();
        
        nextInputCodeIndex++;
        UpdateQTEUI();
    }

    private void SuccessQTE()
    {
        SuccessFeedBack();
        UpdateQTEBaseSequence();
        CraftingManager.Instance().AddIngredientSuccess();
        StopQTE();
    }

    private void FailQTE()
    {
        ErrorFeedBack();
        CraftingManager.Instance().AddIngredientFail();
        StopQTE();
    }

    private void TreatInputDown()
    {
        if(currentInput != null)
            return;

        if(Input.GetButtonDown("ButtonA"))
        {
            currentInput = "ButtonA";
            isCurrentInputAnAxis = false;
        }
        if(Input.GetButtonDown("ButtonB"))
        {
            currentInput = "ButtonB";
            isCurrentInputAnAxis = false;
        }
        if(Input.GetButtonDown("ButtonX"))
        {
            currentInput = "ButtonX";
            isCurrentInputAnAxis = false;
        }
        if(Input.GetButtonDown("ButtonY"))
        {
            currentInput = "ButtonY";
            isCurrentInputAnAxis = false;
        }
        if(-1.0f == Input.GetAxis("DPadX"))
        {
            currentInput = "DPadLeft";
            currentInputAxis = "DPadX";
            isCurrentInputAnAxis = true;
        }
        if(1.0f == Input.GetAxis("DPadX"))
        {
            currentInput = "DPadRight";
            currentInputAxis = "DPadX";
            isCurrentInputAnAxis = true;
        }
        if(-1.0f == Input.GetAxis("DPadY"))
        {
            currentInput = "DPadDown";
            currentInputAxis = "DPadY";
            isCurrentInputAnAxis = true;
        }
        if(1.0f == Input.GetAxis("DPadY"))
        {
            currentInput = "DPadUp";
            currentInputAxis = "DPadY";
            isCurrentInputAnAxis = true;
        }
        if(Input.GetButtonDown("BumperLeft"))
        {
            currentInput = "BumperLeft";
            isCurrentInputAnAxis = false;
        }
        if(Input.GetButtonDown("BumperRight"))
        {
            currentInput = "BumperRight";
            isCurrentInputAnAxis = false;
        }

        if(currentInput != null)
            Debug.Log("TreatInputDown : " + currentInput);
    }

    private void TreatInputUp()
    {
        if(currentInput == null || !isCurrentInputTreated)
            return;
        
        if(isCurrentInputAnAxis && 0.0f == Input.GetAxis(currentInputAxis) || !isCurrentInputAnAxis && Input.GetButtonUp(currentInput))
        {
            Debug.Log("TreatInputUp");
            ResetCurrentInput();
        }
    }

    private void TreatCurrentInput()
    {
        if(currentInput == null || isCurrentInputTreated)
            return;

        Debug.Log("TreatCurrentInput");
        isCurrentInputTreated = true;

        if(currentSequence.GetAtAsString(nextInputCodeIndex).Equals(currentInput))
        {
            Debug.Log("Good Input");
            ProgressQTE();

            if(currentSequence.GetSize() == nextInputCodeIndex)
                SuccessQTE();
        }
        else
        {
            Debug.Log("Bad input");
            FailQTE();
        }
    }

    private void ResetCurrentInput()
    {
        Debug.Log("ResetCurrentInput");
        currentInput = null;
        isCurrentInputTreated = false;
    }

    private void UpdateQTEBaseSequence()
    {
        for(int i = 0; i < currentDifficulty; i++)
        {
            QTESequence.QTEInput newInput = GetRandomQTEInput();
            baseSequence.Append(newInput);
        }
    }

    private QTESequence.QTEInput GetRandomQTEInput()
    {
        int rand = Random.Range(0, 10);

        switch(rand)
        {
            case 0:     return QTESequence.QTEInput.ButtonA;
            case 1:     return QTESequence.QTEInput.ButtonB;
            case 2:     return QTESequence.QTEInput.ButtonX;
            case 3:     return QTESequence.QTEInput.ButtonY;
            case 4:     return QTESequence.QTEInput.DPadLeft;
            case 5:     return QTESequence.QTEInput.DPadRight;
            case 6:     return QTESequence.QTEInput.DPadDown;
            case 7:     return QTESequence.QTEInput.DPadUp;
            case 8:     return QTESequence.QTEInput.BumperLeft;
            case 9:     return QTESequence.QTEInput.BumperRight;
            default:    return QTESequence.QTEInput.ButtonA;
        }
    }

    private void UpdateQTEUI()
    {
        int blankInputCount = 0;
        for(int i = 0; i < nextInputCodeIndex % maxDisplayInputCount; i++)
        {
            inputImages[i].sprite = noInputSprite;
            blankInputCount++;
        }

        int displayInputCount = 0;
        for(int i = blankInputCount; i < Mathf.Min(currentSequence.GetSize() - nextInputCodeIndex + blankInputCount, maxDisplayInputCount); i++)
        {
            inputImages[i].sprite = GetInputSprite(currentSequence.GetAt(displayInputCount + nextInputCodeIndex));
            displayInputCount++;
        }

        for(int i = blankInputCount +displayInputCount; i < maxDisplayInputCount; i++)
        {
            inputImages[i].sprite = noInputSprite;
        }

        if(currentSequence.GetSize() != 0)
        {
            int totalLineCount = currentSequence.GetSize() / maxDisplayInputCount + (currentSequence.GetSize() % maxDisplayInputCount > 0 ? 1 : 0);
            int passedLine = nextInputCodeIndex / maxDisplayInputCount;
            int lineRemainingCount = totalLineCount - passedLine;
            remainingLineText.text = lineRemainingCount + "x";
        }
        else
            remainingLineText.text = "";
    }

    private void ProgressFeedBack()
    {
        Debug.Log("ProgressQTE");
        audioSource.PlayOneShot(soundProgress);
        Instantiate(particuleProgress, inputImages[nextInputCodeIndex % maxDisplayInputCount].transform.position, Quaternion.identity);
    }

    private void ErrorFeedBack()
    {
        Debug.Log("FailQTE");
        audioSource.PlayOneShot(soundError);
        Instantiate(particuleError, inputImages[nextInputCodeIndex % maxDisplayInputCount].transform.position, Quaternion.identity);
    }

    private void SuccessFeedBack()
    {
        Debug.Log("SuccessQTE");
        audioSource.PlayOneShot(soundSuccess);
        Instantiate(particuleSuccess, inputImages[(nextInputCodeIndex - 1) % maxDisplayInputCount].transform.position, Quaternion.identity);
    }

    private Sprite GetInputSprite(QTESequence.QTEInput input)
    {
        return completeInputSpriteList[(int)input];
    }

    private bool IsLastInputForCurrentSequence()
    {
        return nextInputCodeIndex == currentSequence.GetSize() - 1;
    }
}
