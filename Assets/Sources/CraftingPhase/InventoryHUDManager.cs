﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventoryHUDManager : MonoBehaviour
{
    private static InventoryHUDManager sInstance;

    public Image[] listIngredientHUD = new Image[5];

    public Ingredient[] listIngredient = new Ingredient[5];

    private GameObject[] itemGO = new GameObject[5];
    public Image[] difficultyImages;
    public Sprite[] difficultySprite;
    public Sprite difficultyNotSelectedSprite;

    private bool isHUDVisible;
    private int lastSelected;


    public static InventoryHUDManager Instance()
    {
        return sInstance;

    }

    void Awake()
    {
        sInstance = this;
        Transform allItem = transform.Find("Frame/Background");
        for(int i=0;i<5;i++)
        {
            itemGO[i] = allItem.GetChild(i).gameObject;
            listIngredientHUD[i] = allItem.GetChild(i).GetChild(1).gameObject.GetComponent<Image>();
            listIngredientHUD[i].transform.parent.gameObject.SetActive(false);
        }
        isHUDVisible = true;
        lastSelected = 0;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isHUDVisible)
        {
            for(int i = 0; i < 5; i++)
            {
                difficultyImages[i].sprite = difficultyNotSelectedSprite;
                difficultyImages[i].gameObject.SetActive(i < PlayerManager.Instance().numberActualItem);
            }

            GameObject go = EventSystem.current.currentSelectedGameObject;
            if(go != null)
            {
                for(int i = 0; i < 5; i++)
                {
                    if(go == itemGO[i])
                    {
                        difficultyImages[i].sprite = difficultySprite[listIngredient[i].difficultyLevel - 1];
                    }
                }
            }
        }
        else
        {
            for(int i = 0; i < 5; i++)
            {
                difficultyImages[i].gameObject.SetActive(false);
            }
        }
    }

    public void UpdateHUD()
    {
        for(int i=PlayerManager.Instance().numberActualItem-1;i>=0;i--)
        {
            listIngredient[i] = PlayerManager.Instance().ingredientsPlayerPool[i];
            listIngredientHUD[i].sprite = listIngredient[i].artwork;
            listIngredientHUD[i].transform.parent.gameObject.SetActive(true);
        }
        EventSystem.current.SetSelectedGameObject(listIngredientHUD[0].transform.parent.gameObject);
        EventSystem.current.SetSelectedGameObject(listIngredientHUD[1].transform.parent.gameObject);
        EventSystem.current.SetSelectedGameObject(listIngredientHUD[lastSelected].transform.parent.gameObject);
        isHUDVisible = true;
    }

    public void ShowHUD()
    {
        transform.Find("Frame/Background").gameObject.SetActive(true);
        EventSystem.current.SetSelectedGameObject(listIngredientHUD[0].transform.parent.gameObject);
        EventSystem.current.SetSelectedGameObject(listIngredientHUD[1].transform.parent.gameObject);
        EventSystem.current.SetSelectedGameObject(listIngredientHUD[lastSelected].transform.parent.gameObject);
        isHUDVisible = true;
    }

    public void HideHUD()
    {
        transform.Find("Frame/Background").gameObject.SetActive(false);
        isHUDVisible = false;
    }

    public void OnClick(int id)
    {
        lastSelected = id;
        CraftingManager.Instance().AddIngredientToPotion(listIngredient[id]);
    }
}
