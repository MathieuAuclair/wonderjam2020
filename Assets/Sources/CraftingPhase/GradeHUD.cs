﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GradeHUD : MonoBehaviour
{

    public Image strenghtGameObject;
    public Image strenghtRatioGameObject;
    public Image dexterityGameObject;
    public Image dexterityRatioGameObject;
    public Image constitutionGameObject;
    public Image constitutionRatioGameObject;
    public Image speedGameObject;
    public Image speedRatioGameObject;
    public Sprite gradeA;
    public Sprite gradeB;
    public Sprite gradeC;
    public Sprite gradeD;
    public Sprite gradeE;
    public Sprite gradeF;

    public Sprite winRatio;

    public Sprite looseRatio;

    public Sprite equalRatio;

    public float minRatioA;
    public float minRatioB;
    public float minRatioC;
    public float minRatioD;
    private float minRatioE = 0.0f;

    private float oldRatioStrengh;
    private float oldRatioDexterity;
    private float oldRatioConstitution;
    private float oldRatioSpeed;

    private bool visible = false;

    public float timeShowGradeRatioResult;
    private float timeRemain;

    public Stats enemyStats;
    // Start is called before the first frame update
    void Start()
    {
        enemyStats.strength =  BattleManager.Instance().getEnemy().Strength;
        enemyStats.dexterity =  BattleManager.Instance().getEnemy().Dexterity;
        enemyStats.constitution =  BattleManager.Instance().getEnemy().Constitution;
        enemyStats.speed =  BattleManager.Instance().getEnemy().Speed;
        strenghtGameObject.sprite = gradeF;
        dexterityGameObject.sprite = gradeF;
        constitutionGameObject.sprite = gradeF;
        speedGameObject.sprite = gradeF;
        strenghtRatioGameObject.gameObject.SetActive(false);
        dexterityRatioGameObject.gameObject.SetActive(false);
        constitutionRatioGameObject.gameObject.SetActive(false);
        speedRatioGameObject.gameObject.SetActive(false);
        timeRemain=timeShowGradeRatioResult;

    }



    public void UpdateGrade()
    {
        var newPotion = CraftingManager.Instance().GetPotionStats();

        float newRatioStrengh = newPotion.strength / enemyStats.strength;
        ChangeStat(newRatioStrengh,oldRatioStrengh,strenghtGameObject,strenghtRatioGameObject);

        float newRatioDexterity = newPotion.dexterity / enemyStats.dexterity;
        ChangeStat(newRatioDexterity,oldRatioDexterity,dexterityGameObject,dexterityRatioGameObject);

        float newRatioConstitution = newPotion.constitution / enemyStats.constitution;
        ChangeStat(newRatioConstitution,oldRatioConstitution,constitutionGameObject,constitutionRatioGameObject);

        float newRatioSpeed = newPotion.speed / enemyStats.speed;
        ChangeStat(newRatioSpeed,oldRatioSpeed,speedGameObject,speedRatioGameObject);  

        visible = true; 
        
    }

    void Update()
    {
        if(visible)
        {
            timeRemain -= Time.deltaTime;
            strenghtRatioGameObject.gameObject.SetActive(true);
            dexterityRatioGameObject.gameObject.SetActive(true);
            constitutionRatioGameObject.gameObject.SetActive(true);
            speedRatioGameObject.gameObject.SetActive(true);
            if(timeRemain < 0)
            {
                visible = false;
                timeRemain= timeShowGradeRatioResult;
            }
        }else{
            strenghtRatioGameObject.gameObject.SetActive(false);
            dexterityRatioGameObject.gameObject.SetActive(false);
            constitutionRatioGameObject.gameObject.SetActive(false);
            speedRatioGameObject.gameObject.SetActive(false);
        }
    }

    private void ChangeStat(float newRatio,float oldRatio,Image imageGameObject,Image ratioGameObject)
    {

        if(newRatio>oldRatio)
        {
            ratioGameObject.sprite = winRatio;
        }else if(newRatio<oldRatio)
        {
            ratioGameObject.sprite = looseRatio;
        }else
        {
            ratioGameObject.sprite = equalRatio;
        }

        if(newRatio>minRatioA)
        {
            imageGameObject.sprite = gradeA;
            return;
        }
        if(newRatio>minRatioB)
        {
            imageGameObject.sprite = gradeB;
            return;
        }
        if(newRatio>minRatioC)
        {
            imageGameObject.sprite = gradeC;
            return;
        }
        if(newRatio>minRatioD)
        {
            imageGameObject.sprite = gradeD;
            return;
        }
        if(newRatio>minRatioE)
        {
            imageGameObject.sprite = gradeE;
            return;
        }
        imageGameObject.sprite = gradeF;
    }
}
