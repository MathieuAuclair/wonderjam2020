﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroManager : MonoBehaviour
{
    public float tutorialDuration = 2.0f;
    private float tutorialRemainingTime;
    // Start is called before the first frame update
    void Start()
    {
        tutorialRemainingTime = tutorialDuration;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("ButtonA"))
        {
            GoToMainMenu();
            return;
        }

        tutorialRemainingTime -= Time.deltaTime;
        if(tutorialRemainingTime <= 0.0f)
        {
            GoToMainMenu();
        }
    }

    private void GoToMainMenu()
    {
        ChangeSceneManager.Instance().ChangeScene(Scene.MainMenuScene);
    }
}
