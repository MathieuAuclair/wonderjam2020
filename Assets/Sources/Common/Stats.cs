﻿[System.Serializable]
public class Stats
{
    public float strength = 0.0f;
    public float dexterity = 0.0f;
    public float constitution = 0.0f;
    public float speed = 0.0f;
}
