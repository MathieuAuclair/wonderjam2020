﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene 
{
    private Scene(string value) { Value = value; }
    public string Value { get; set; }

    public static Scene MainMenuScene   { get { return new Scene("MainMenu"); } }
    public static Scene IntroScene   { get { return new Scene("Intro"); } }
    public static Scene Game    { get { return new Scene("Game"); } }
    public static Scene BattlePhaseScene { get { return new Scene("BattlePhase"); } }
    public static Scene RewardScene   { get { return new Scene("Reward"); } }
    public static Scene VictoryScene   { get { return new Scene("Victory"); } }
    public static Scene DefeatScene   { get { return new Scene("Defeat"); } }

    //public static Scene Lucas   { get { return new Scene("Lucas"); } }
}

public class ChangeSceneManager : MonoBehaviour
{
    private static ChangeSceneManager sInstance;

    public static ChangeSceneManager Instance()
    {
        return sInstance;
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        sInstance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeScene(Scene newScene){
		//GameControl.control.Save ();
		//PlayerPrefs.SetString("SceneToLoad", NomDeLaScene);
		SceneManager.LoadScene(newScene.Value);	
	}
}
