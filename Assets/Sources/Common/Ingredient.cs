﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CauldronEffect
{
    Strength,
    Dexterity,
    Constitution,
    Speed
}

[CreateAssetMenu(fileName = "Data", menuName = "Data/Ingredient", order = 1)]
public class Ingredient : ScriptableObject
{
    public string name;
    public Sprite artwork;
    public int difficultyLevel; // 1, 2 or 3
    public Stats stats = new Stats();
    public QTESequence qteSequence = new QTESequence();
    public GameObject model3DPrefab;
    public CauldronEffect cauldronEffect;
}
