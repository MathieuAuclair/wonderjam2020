﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temp : MonoBehaviour
{
    bool finish = false;

    private static System.Random rnd = new System.Random();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!finish)
        {
            //CraftingManager.Instance().StartCrafting(60.0f);
            finish= true;
        }else{
            if(Input.GetKeyDown(KeyCode.S))
            {
                //ChangeSceneManager.Instance().ChangeScene(Scene.Lucas);
            }
            if(Input.GetKeyDown(KeyCode.I))
            {
                PlayerManager.Instance().InitIngredientPool();
                InventoryHUDManager.Instance().UpdateHUD();
            }
            if(Input.GetKeyDown(KeyCode.H))
            {
                InventoryHUDManager.Instance().UpdateHUD();
            }
            if(Input.GetKeyDown(KeyCode.W))
            {
                ChangeSceneManager.Instance().ChangeScene(Scene.RewardScene);
            }
            if(Input.GetKeyDown(KeyCode.Keypad0))
            {
                RewardManager.Instance().OnClick(0);
            }
            if(Input.GetKeyDown(KeyCode.Keypad1))
            {
                RewardManager.Instance().OnClick(1);              
            }
            if(Input.GetKeyDown(KeyCode.Keypad2))
            {
                RewardManager.Instance().OnClick(2);
            }
        }
    }
}
