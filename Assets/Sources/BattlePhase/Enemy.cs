﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float Strength = 2.5f;
    public float Dexterity = 2.0f;
    public float Constitution = 4.0f;
    public float Speed = 1.0f; 
    public float Health;
    public float boostModifier = 0.15f;
    
    void Start() {
        Health = Constitution;
    }

    public bool IsDead() {
        return Health <= 0.0f;
    }

    public void TakeDamage(float damage) {
        Health -= damage;
    }

    public void Boost(int round) {
        Strength += boostModifier * round;
        Dexterity += boostModifier * round;
        Constitution += boostModifier * round;
        Speed += boostModifier * round;
    }
}
