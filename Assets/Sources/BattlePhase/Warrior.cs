﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warrior : MonoBehaviour
{
    public float Strength = 3.0f;
    public float Dexterity = 2.0f;
    public float Constitution = 4.0f;
    public float Speed = 1.0f; 
    public float Health;
    
    void Start() {
        Health = Constitution;
    }

    public bool IsDead() {
        return Health <= 0.0f;
    }

    public void TakeDamage(float damage) {
        Health -= damage;
    }

    public void Drink(Stats stats) {
        Strength += stats.strength;
        Dexterity += stats.dexterity;
        Constitution += stats.constitution;
        Speed += stats.speed;

        Health = Constitution;
    }
}
