﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour
{
    public Warrior warrior;
    public Enemy enemy;
    //Enemy won   if battleResult == 1
    //Warrior won if battleResult == 2
    //In progress if battleResult == 0 
    //Not started if battleResult == -1 
    public int battleResult = -1;
    private static BattleManager sInstance;

    public static BattleManager Instance()
    {
        return sInstance;
    }

    void Awake()
    {
        sInstance = this;
    }

    public Warrior getWarrior() {
        return warrior;
    }

    public Enemy getEnemy() {
        return enemy;
    }

    public void PrepareBattle(Stats stats) {
        enemy.Boost(PlayerManager.Instance().CurrentRound);
        GivePotionToWarrior(stats);
    }

    public void Fight() {
        if(battleResult != -1) {
            Debug.Log("cant Fight, you need to prepare battle first");
            return;
        }

        float warriorPower = CalculateDamage(warrior.Strength, warrior.Dexterity, warrior.Constitution, warrior.Speed, enemy.Dexterity);
        float enemyPower = CalculateDamage(enemy.Strength, enemy.Dexterity, enemy.Constitution, enemy.Speed, warrior.Dexterity);

        if(warriorPower > enemyPower)
            PlayerManager.Instance().WarriorWin();
        else
            PlayerManager.Instance().EnemyWin();
    }

    void GivePotionToWarrior(Stats stats)
    {
        warrior.Drink(stats);
    }

    float CalculateDamage(float str, float dex, float cons, float speed, float dexOpponent) {
        return (str + Mathf.Max(0.0f, dex - dexOpponent)) * speed + cons;
    }
}