﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardManager : MonoBehaviour
{
  private static System.Random rnd = new System.Random();
  private static RewardManager sInstance;

  public int numberElementToChoose;

  public List<Ingredient> listChooseIngredient = new List<Ingredient>();
  public Text resultText;


  private Transform allItem;
  public static RewardManager Instance()
  {
    return sInstance;
  }

  void Awake()
  {

    sInstance = this;
    HandleGameOver();
  }

  // Start is called before the first frame update
  void Start()
  {
    allItem = transform.Find("Frame/Background");

    for (int i = 0; i < numberElementToChoose; i++)
    {
      int r = rnd.Next(PlayerManager.Instance().ingredientPoolTotal.Count);
      listChooseIngredient.Add(PlayerManager.Instance().ingredientPoolTotal[r]);
      allItem.GetChild(i).GetChild(1).gameObject.GetComponent<Image>().sprite = listChooseIngredient[i].artwork;
    }

    if(PlayerManager.Instance().Winner == 1)  // enemy
      resultText.text = "Defeat !";
    else
      resultText.text = "Victory !";
  }

  void UpdateHUDChooseElement()
  {

  }

  public void OnClick(int id)
  {
    PlayerManager.Instance().AddIngredientToAvailablePool(listChooseIngredient[id]);
    listChooseIngredient.Clear();
    LoadNewGameLoop();
  }

  void HandleGameOver()
  {
    if (PlayerManager.Instance().isGameOver())
    {
      Debug.Log("GAME OVER !");
      ChangeSceneManager.Instance().ChangeScene(Scene.DefeatScene);
    }

    if (PlayerManager.Instance().isGameWinning())
    {
      Debug.Log("VICTORY !");
      ChangeSceneManager.Instance().ChangeScene(Scene.VictoryScene);
    }
  }

  void LoadNewGameLoop()
  {
    if (!PlayerManager.Instance().isGameOver() && !PlayerManager.Instance().isGameWinning())
    {
      Debug.Log("Continue Game ...");
      PlayerManager.Instance().StartTurn();
    }
  }
}