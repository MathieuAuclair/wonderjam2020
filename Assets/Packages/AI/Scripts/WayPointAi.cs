﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Packages.BobbyJohanssonPack.AI.Library
{
    public class WayPointAi : MonoBehaviour
    {
        public GameObject Target;
        public List<GameObject> WayPoints;

        public bool IsPatrolRandom;
        public float DistanceToHaveReachedCheckPoint = 1f;
        public float SpeedBoostOnTargetDetection = 5f;
        public bool IsInRangeOfTarget;

        private NavMeshAgent agent;
        private int indexDestination;
        private bool hasBoost;


        void Start()
        {
            if (Target)
            {
                Debug.Log($"Your AI has no target set to chase, looking for patrol");
            }

            if (WayPoints == null || !WayPoints.Any() || WayPoints.FirstOrDefault() == null)
            {
                Debug.Log($"Your AI {name} has no waypoint to patrol, going for the targets!");
                WayPoints.Add(Target);
            }

            indexDestination = 0;
            agent = GetComponent<NavMeshAgent>();
            agent.SetDestination(WayPoints[indexDestination].transform.position);
        }

        void Update()
        {
            if (IsInRangeOfTarget)
            {
                agent.SetDestination(Target.transform.position);
            }

            if (agent.remainingDistance < DistanceToHaveReachedCheckPoint)
            {
                SetNextDestination();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (Target == other.gameObject)
            {
                IsInRangeOfTarget = true;
                GiveSpeedBoost();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (Target == other.gameObject)
            {
                IsInRangeOfTarget = false;
                RemoveSpeedBoost();
            }
        }

        private void GiveSpeedBoost()
        {
            if (hasBoost)
            {
                return;
            }

            hasBoost = true;
            agent.speed *= SpeedBoostOnTargetDetection;
            agent.acceleration *= SpeedBoostOnTargetDetection;
        }

        private void RemoveSpeedBoost()
        {
            if (!hasBoost)
            {
                return;
            }

            hasBoost = false;
            agent.speed /= SpeedBoostOnTargetDetection;
            agent.acceleration /= SpeedBoostOnTargetDetection;
        }

        private void SetNextDestination()
        {
            if (IsPatrolRandom)
            {
                SetRandomDestination();
            }
            else
            {
                SetNextDestinationWithNextCheckPoint();
            }
        }

        private void SetRandomDestination()
        {
            indexDestination = Random.Range(0, WayPoints.Count - 1);
        }

        private void SetNextDestinationWithNextCheckPoint()
        {
            indexDestination++;

            if (indexDestination >= WayPoints.Count)
            {
                indexDestination = 0;
            }

            agent.SetDestination(WayPoints[indexDestination].transform.position);
        }
    }
}