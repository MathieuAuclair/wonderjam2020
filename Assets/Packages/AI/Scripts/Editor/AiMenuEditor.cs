﻿using System.Linq;
using Assets.Packages.NavMeshComponents.Scripts;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Packages.BobbyJohanssonPack.AI.Editor
{
    public class MenuEditorAi : EditorWindow
    {
        [MenuItem("BobbyJohansson\'s Tools/AI/Create navigation mesh")]
        private static void CreateNavMesh()
        {
            GameObject navMesh = new GameObject { name = "AiManager" };

            if (Selection.activeTransform == null)
            {
                Debug.LogError("You must select the maps GameObject root element in the hierarchy!");
                return;
            }

            navMesh.transform.parent = Selection.activeTransform;
            navMesh.AddComponent<NavMeshSurface>();
            navMesh.GetComponent<NavMeshSurface>().BuildNavMesh();
        }

        [MenuItem("BobbyJohansson\'s Tools/AI/Movement/Set game object as obstacle")]
        private static void SetAsObstacle()
        {
            if (!Selection.gameObjects.Any())
            {
                Debug.LogError("You must select a GameObject");
                return;
            }

            foreach (var gameObjectObstacle in Selection.gameObjects)
            {
                gameObjectObstacle.AddComponent<NavMeshObstacle>();
                gameObjectObstacle.GetComponent<NavMeshObstacle>().carving = true;
            }
        }

        [MenuItem("BobbyJohansson\'s Tools/AI/Movement/Add jump link to selected AiManager")]
        private static void AddJumpLink()
        {
            if (Selection.activeTransform == null || Selection.activeTransform.GetComponent<NavMeshSurface>() == null)
            {
                Debug.LogError("You must select an AiManager in the hierarchy!");
                return;
            }

            var jumpLink = new GameObject();
            jumpLink.AddComponent<NavMeshLink>();
            jumpLink.name = "AiJumpLink";
            jumpLink.transform.parent = Selection.activeTransform;
        }

        [MenuItem("BobbyJohansson\'s Tools/Gizmos/Add arrow")]
        private static void AddArrowGizmo()
        {
            Selection.activeTransform.gameObject.AddComponent<ArrowGizmo>();
        }

        [MenuItem("BobbyJohansson\'s Tools/Gizmos/Add radius")]
        private static void AddRadiusGizmo()
        {
            Selection.activeTransform.gameObject.AddComponent<RadiusGizmo>();
        }

        [MenuItem("BobbyJohansson\'s Tools/Gizmos/Add box")]
        private static void AddBoxGizmo()
        {
            Selection.activeTransform.gameObject.AddComponent<CubeGizmo>();
        }
    }
}