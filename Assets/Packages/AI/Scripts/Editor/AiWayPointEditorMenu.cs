﻿using Assets.Packages.BobbyJohanssonPack.AI.Library;
using Assets.Packages.NavMeshComponents.Scripts;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Packages.BobbyJohanssonPack.AI.Editor
{
    class AiWayPointEditorMenu : EditorWindow
    {
        static GameObject selection;
        static GameObject bot;

        static string botName;
        static bool isPatrolSettingsEnabled;
        static bool isPatrolRandom;
        static bool isChaseSettingsEnabled = false;
        static float chaseRadiusRange;
        static int countOfWaypoint = 1;
        static int countOfTargets;
        static bool isCreationSuccessful;

        static AiWayPointEditorMenu window;

        static Vector2 scrollPosition = new Vector2();

        private static GameObject target = new GameObject();
        private static List<GameObject> waypoints = new List<GameObject>();

        [MenuItem("BobbyJohansson\'s Tools/AI/Bot/Add AI with way point")]
        private static void CreateWayPointAgent()
        {
            if (Selection.activeTransform == null || Selection.activeTransform.GetComponent<NavMeshSurface>() == null)
            {
                Debug.LogError("You must select the AiManager GameObject root element in the hierarchy!");
                return;
            }

            selection = Selection.activeTransform.gameObject;
            CreateBotGameObject();

            window = (AiWayPointEditorMenu)GetWindow(typeof(AiWayPointEditorMenu));
            window.titleContent = new GUIContent("WayPoint Ai");
            window.name = "WayPoint Ai";
            window.ShowPopup();
        }

        [MenuItem("BobbyJohansson\'s Tools/AI/Bot/Add AI with someone to fuck up")]
        private static void CreateWayFuckFestAgent()
        {
            if (Selection.activeTransform == null)
            {
                Debug.LogError("Select a fucking gameobject you dip shit!");
                return;
            }

            selection = Selection.activeTransform.gameObject;

            selection.AddComponent<FuckFestAi>();
            selection.AddComponent<NavMeshAgent>();
            selection.AddComponent<Rigidbody>();
        }

        private void OnDestroy()
        {
            if (bot != null && !isCreationSuccessful)
            {
                DestroyImmediate(bot);
                bot = null;
            }

            if (window != null && isCreationSuccessful)
            {
                window.Close();
                DestroyImmediate(window);
                window = null;
            }
        }

        void OnGUI()
        {
            BuildAndUpdateGui();

            if (!GUILayout.Button("Build Object"))
            {
                return;
            }

            if (isChaseSettingsEnabled && target == null)
            {
                Debug.LogWarning("If you want to use Player Chase, select at least a single target GameObject!");
                return;
            }

            bot.name = botName;
            bot.GetComponent<SphereCollider>().radius = chaseRadiusRange;
            isCreationSuccessful = true;

            window.Close();
            window = null;
        }

        private static void CreateBotGameObject()
        {
            bot = new GameObject();
            bot.transform.parent = selection.transform;
            bot.AddComponent<WayPointAi>();
            bot.AddComponent<NavMeshAgent>();
            bot.AddComponent<RadiusGizmo>();
            bot.AddComponent<Rigidbody>();
            bot.AddComponent<SphereCollider>();
            bot.GetComponent<SphereCollider>().isTrigger = true;
            var ai = bot.GetComponent<WayPointAi>();
            ai.Target = target;
            ai.WayPoints = waypoints;
        }

        private void BuildAndUpdateGui()
        {
            // Generic settings
            EditorStyles.label.wordWrap = true;
            EditorStyles.label.stretchWidth = true;
            EditorStyles.label.fontStyle = FontStyle.Normal;
            EditorStyles.label.padding = new RectOffset(10, 10, 10, 10);
            GUIStyle defaultLabelStyle = new GUIStyle(EditorStyles.label);

            EditorStyles.label.padding = new RectOffset(10, 0, 0, 0);
            GUIStyle paddingLeftLabelStyle = new GUIStyle(EditorStyles.label);

            //Start of window
            GUILayout.Label("Create way point AI", EditorStyles.boldLabel);
            scrollPosition = GUILayout.BeginScrollView(scrollPosition, new GUILayoutOption[] { });

            GUILayout.BeginHorizontal(new GUILayoutOption[] { });
            GUILayout.Label("Bot name:");
            botName = EditorGUILayout.TextField(botName);
            GUILayout.EndHorizontal();

            EditorGUILayout.BeginVertical();
            GUILayout.Label("AI Target to chase", paddingLeftLabelStyle);
            GUILayout.Label(
               "Give your bot a target to chase",
               defaultLabelStyle,
               new GUILayoutOption[] { }
            );

            target = (GameObject)EditorGUILayout.ObjectField(target, typeof(Object), true);

            GUILayout.Space(10);

            isPatrolSettingsEnabled = EditorGUILayout.BeginToggleGroup(
                "Way point patrol Settings",
                isPatrolSettingsEnabled
            );

            GUILayout.Label(
               "To use a way point Ai, you simply need to " +
               "add empty gameObject to you WayPointAiPath and " +
               "he will patrol through those gameObjects",
               defaultLabelStyle,
               new GUILayoutOption[] { }
            );

            GUILayout.BeginHorizontal(new GUILayoutOption[] { });
            GUILayout.Label("Is patrol random:", paddingLeftLabelStyle);
            isPatrolRandom = EditorGUILayout.Toggle(isPatrolRandom);
            GUILayout.EndHorizontal();

            GUILayout.Space(10);

            SpawnDropDownGameObjectSelector(
                "Count of waypoint",
                "The waypoint are empty object in the map to set a patrol with your AI, " +
                "if the AI pass near the player, he will stop his patrol and chase the target. " +
                "Check under the resource folder in AI, there's a prefab for that with Gizmo",
                ref countOfWaypoint,
                ref waypoints
            );

            GUILayout.Space(10);

            GUILayout.BeginHorizontal(new GUILayoutOption[] { });
            GUILayout.Label("Radius of detection", paddingLeftLabelStyle);
            chaseRadiusRange = EditorGUILayout.Slider(chaseRadiusRange, 0f, 10f);

            RadiusGizmo gizmo = null;

            if (bot != null)
            {
                gizmo = bot.GetComponent<RadiusGizmo>();
            }

            if (gizmo != null)
            {
                gizmo.Radius = chaseRadiusRange;
                UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
            }

            GUILayout.EndHorizontal();
            EditorGUILayout.EndToggleGroup();
            GUILayout.Space(10);
            GUILayout.EndScrollView();
        }

        private void SpawnDropDownGameObjectSelector(string title, string hint, ref int countOfField, ref List<GameObject> gameObjects)
        {
            // Generic settings
            EditorStyles.label.wordWrap = true;
            EditorStyles.label.stretchWidth = true;
            EditorStyles.label.padding = new RectOffset(10, 10, 10, 10);
            EditorStyles.label.fontStyle = FontStyle.Normal;
            GUIStyle defaultLabelStyle = new GUIStyle(EditorStyles.label);

            EditorStyles.label.padding = new RectOffset(10, 0, 0, 0);
            EditorStyles.label.fontStyle = FontStyle.Bold;
            GUIStyle paddingLeftLabelStyle = new GUIStyle(EditorStyles.label);

            //Start of dropdown 
            EditorGUILayout.BeginVertical();
            GUILayout.Label(title, paddingLeftLabelStyle);
            GUILayout.Label(
               hint,
               defaultLabelStyle,
               new GUILayoutOption[] { }
            );

            countOfField = EditorGUILayout.IntField(countOfField, new GUILayoutOption[] { });

            ResetDropDownGameObjectSelector(countOfField, ref gameObjects);

            for (int i = 0; i < gameObjects.Count; i++)
            {
                gameObjects[i] = (GameObject)EditorGUILayout.ObjectField(gameObjects[i], typeof(Object), true);
            }

            EditorGUILayout.EndVertical();
        }

        private void ResetDropDownGameObjectSelector(int countOfField, ref List<GameObject> gameObjects)
        {
            if (gameObjects.Count != countOfField)
            {
                if (gameObjects.Count > countOfField)
                {
                    gameObjects = gameObjects.Take(countOfField).ToList();
                }

                for (int i = gameObjects.Count; i < countOfField; i++)
                {
                    gameObjects.Add(null);
                }
            }
        }

        private static void SetChaseParameters()
        {
            bot.GetComponent<WayPointAi>().IsPatrolRandom = isPatrolRandom;
            bot.GetComponent<WayPointAi>().Target = target;

            if (!isChaseSettingsEnabled)
            {
                return;
            }
        }
    }
}