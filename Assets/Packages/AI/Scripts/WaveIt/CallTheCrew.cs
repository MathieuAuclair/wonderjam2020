﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.Packages.BobbyJohanssonPack.AI.Library
{
    [System.Serializable]
    public class AiCharacterTeam
    {
        public bool ForceVictory = false;
        public int CountOfMember = 5;
        public List<Transform> SpawnPoint = new List<Transform>();
        public List<GameObject> Prefabs = new List<GameObject>();
        public List<GameObject> Members { get; set; } = new List<GameObject>();
    }

    public class CallTheCrew : MonoBehaviour
    {
        public List<AiCharacterTeam> Teams;
        public GameObject LoadingUi;

        private void OnValidate()
        {
            if (Teams == null)
            {
                Debug.LogError("You must have at least a single team!");
            }
        }

        void Start()
        {
            if (PlayerManager.Instance() != null)
            {
                Teams.FirstOrDefault().ForceVictory = (PlayerManager.Instance().Winner == 2);
                Teams.LastOrDefault().ForceVictory = (PlayerManager.Instance().Winner == 1);
            }

            Time.timeScale = 0.0001f;
            StartCoroutine(WaitAFuckingSecond());
        }

        public void LateUpdate()
        {
            var isThereOnlyOneTeam = Teams
                .Select(team => team.Members.Any(member => member.GetComponent<AiCharacter>().IsAlive))
                .Where(isAlive => isAlive == true)
                .Count() > 1;

            if (isThereOnlyOneTeam)
            {
                return; // The fight is not done!
            }

            WaitAFuckingSecondLikeNotWithFrameSkipLol();
            ChangeSceneManager.Instance()?.ChangeScene(Scene.RewardScene);
        }

        IEnumerator WaitAFuckingSecondLikeNotWithFrameSkipLol()
        {
            yield return new WaitForSeconds(2.5f);
        }

        IEnumerator WaitAFuckingSecond()
        {
            Debug.Log("START LOADING");

            SpawnTheCrew();
            SetPeopleToFuckUp();

            for (var frame = 0; frame < 180; frame++)
            {
                Debug.Log("FRAME " + frame);
                yield return null;
            }

            LoadingUi.SetActive(false);
            Time.timeScale = 1;

            // timeout for the scene in case the AI fuck theirs only one job

            yield return new WaitForSeconds(15);

            Debug.Log("Simulation froze, loading next scene");
            ChangeSceneManager.Instance()?.ChangeScene(Scene.RewardScene);
        }

        private void SetPeopleToFuckUp()
        {
            foreach (var team in Teams)
            {
                foreach (var member in team.Members)
                {
                    member.GetComponent<FuckFestAi>().Enemies = GetAllMembersFromAllTeams()
                        .Where(enemy => enemy.GetComponent<AiCharacter>().Team != member.GetComponent<AiCharacter>().Team)
                        .ToList();
                }
            }
        }

        private void ActivateAllAi()
        {
            GetAllMembersFromAllTeams().ForEach(member => member.SetActive(true));
        }

        private List<GameObject> GetAllMembersFromAllTeams()
        {
            return Teams
            .SelectMany(team => team.Members)
            .ToList();
        }

        private void SpawnTheCrew()
        {
            for (var teamId = 0; teamId < Teams.Count; teamId++)
            {
                for (var i = 0; i < Teams[teamId].CountOfMember; i++)
                {
                    var spawnPoint = Teams[teamId].SpawnPoint[Random.Range(0, Teams[teamId].SpawnPoint.Count())];

                    var member = Instantiate(
                        Teams[teamId].Prefabs[Random.Range(0, Teams[teamId].Prefabs.Count - 1)],
                        spawnPoint.position,
                        Quaternion.identity
                    );

                    member.transform.parent = spawnPoint;

                    var ai = member.GetComponent<AiCharacter>();
                    ai.Team = teamId + 1;

                    //TODO set team stats

                    if (Teams[teamId].ForceVictory && Random.Range(0, Teams[teamId].Members.Count()) <= teamId)
                    {
                        ai.IsInvincible = true;
                    }

                    Teams[teamId].Members.Add(member);
                }

                if (Teams[teamId].ForceVictory)
                {
                    Teams[teamId].Members.FirstOrDefault().GetComponent<AiCharacter>().IsInvincible = true;
                }
            }
        }
    }
}
