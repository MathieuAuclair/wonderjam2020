using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Packages.BobbyJohanssonPack.AI.Library
{
    public class FuckFestAi : MonoBehaviour
    {
        public int DamageMissedShot = 2;
        public int DamagePowerShot = 10;
        public List<GameObject> Enemies = new List<GameObject>();

        private NavMeshAgent agent;
        private int indexDestination;
        private bool isAttackReady = true;

        private void OnValidate()
        {
            if (Enemies.Any(enemy => enemy.GetComponent<AiCharacter>() == null))
            {
                Debug.LogError($"Your AI {name} has some enemies that aren't equiped with AiCharacter, get ur shit togheter man");
            }
        }

        void Start()
        {            
            if (Enemies == null || !Enemies.Any() || Enemies.FirstOrDefault() == null)
            {
                Debug.Log($"Your AI {name} has nobody to fuck up?");
            }

            agent = GetComponent<NavMeshAgent>();
            agent.SetDestination(Enemies[indexDestination].transform.position);
        }

        void LateUpdate()
        {
            if (Enemies[indexDestination].GetComponent<AiCharacter>().IsAlive)
            {
                agent.SetDestination(Enemies[indexDestination].transform.position);
            }
            else
            {
                SetNextDestination();
            }
        }

        private void SetNextDestination()
        {
            if (Enemies.Count - 1 > indexDestination)
            {
                indexDestination++;
            }
        }

        public void OnTriggerStay(Collider other)
        {
            if (other.gameObject.tag != "AiCharacter")
            {
                return;
            }

            var currentAi = GetComponent<AiCharacter>();
            var otherAi = other.gameObject.GetComponent<AiCharacter>();

            if (!otherAi.IsAlive && otherAi.Spine.isKinematic == true)
            {
                KillTheMotherFucker(otherAi);
                return;
            }

            FuckHimUpRealGood(currentAi, otherAi);
        }

        private void FuckHimUpRealGood(AiCharacter current, AiCharacter other)
        {
            if (other.Team == current.Team && current.Team != 0)
            {
                return; // Same team no beef!
            }

            if (!isAttackReady)
            {
                return;
            }

            other.Health -= Random.Range(DamagePowerShot, DamageMissedShot);

            StartCoroutine(WaitAFuckingSecond());
        }

        IEnumerator WaitAFuckingSecond()
        {
            isAttackReady = false;
            yield return new WaitForSeconds(1);
            isAttackReady = true;
        }

        private void KillTheMotherFucker(AiCharacter ai)
        {
            var agent = ai.gameObject.GetComponent<NavMeshAgent>();
            var collider = ai.gameObject.GetComponent<BoxCollider>();

            collider.enabled = false;
            ai.Spine.isKinematic = false;
            agent.isStopped = true;
        }
    }
}