﻿using UnityEngine;
 
public class RadiusGizmo : MonoBehaviour
{
    public float Radius = 5;
    public Color Color = Color.red;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color;
        Gizmos.DrawWireSphere(transform.position, Radius);
    }
}