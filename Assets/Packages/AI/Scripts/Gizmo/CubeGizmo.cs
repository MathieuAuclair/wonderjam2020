﻿using UnityEngine;

public class CubeGizmo : MonoBehaviour
{
    public Vector3 Size = new Vector3(1, 1, 1);
    public Color Color = Color.yellow;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color;
        Gizmos.DrawWireCube(transform.position, Size);
    }
}
