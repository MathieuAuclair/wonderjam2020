﻿using UnityEngine;

public class ArrowGizmo : MonoBehaviour
{
    public Color Color = Color.cyan;

    private void OnDrawGizmos()
    {
        ArrowGizmoHelper.ForGizmo(transform.position, Vector3.down, Color);
    }
}
