using UnityEngine;

public class AiCharacter : MonoBehaviour
{
    public bool IsAlive => GetLifeStatus();
    public Rigidbody Spine;
    public int Team;
    public bool IsInvincible = false;
    public int Health = 100;

    private void OnValidate() 
    {
        if(Spine == null)
        {
            Debug.LogError($"the AI {name} has no spine assigned on it's AiCharacter");
        }    
    }

    private bool GetLifeStatus()
    {
        return Health > 0 || IsInvincible;
    }
}